# OpenCV Haar级联分类器XML文件集合

本仓库提供了一系列用于OpenCV的Haar级联分类器的XML文件，这些文件可用于人脸检测、眼睛检测、身体部位检测等多种计算机视觉任务。以下是本仓库中包含的资源文件列表：

- `haarcascade_eye.xml`：用于检测眼睛的Haar级联分类器。
- `haarcascade_eye_tree_eyeglasses.xml`：用于检测佩戴眼镜的眼睛的Haar级联分类器。
- `haarcascade_frontalcatface.xml`：用于检测正面猫脸的Haar级联分类器。
- `haarcascade_fullbody.xml`：用于检测全身的Haar级联分类器。
- `haarcascade_lefteye_2splits.xml`：用于检测左眼的Haar级联分类器。
- `haarcascade_lowerbody.xml`：用于检测下半身的Haar级联分类器。
- `haarcascade_profileface.xml`：用于检测侧面人脸的Haar级联分类器。
- `haarcascade_smile.xml`：用于检测微笑的Haar级联分类器。
- `haarcascade_upperbody.xml`：用于检测上半身的Haar级联分类器。

## 使用方法

1. 克隆或下载本仓库到本地。
2. 将所需的XML文件复制到你的OpenCV项目中。
3. 使用OpenCV的`cv2.CascadeClassifier`类加载相应的XML文件，并进行目标检测。

示例代码：

```python
import cv2

# 加载Haar级联分类器
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# 读取图像
img = cv2.imread('test.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# 检测人脸
faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5)

# 绘制检测结果
for (x, y, w, h) in faces:
    cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)

# 显示结果
cv2.imshow('Detected Faces', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
```

## 贡献

欢迎提交问题和改进建议。如果你有新的Haar级联分类器XML文件或改进现有文件，请提交PR。

## 许可证

本仓库中的文件遵循OpenCV的许可证。具体信息请参考OpenCV的官方文档。